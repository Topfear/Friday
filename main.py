import re
from typing import Union
from fuzzywuzzy import fuzz

from infi.systray import SysTrayIcon
from commands.commands_map import VA_COMMANDS, VA_COMMANDS_MAP
from commands.va_exit import va_exit
import config
import stt
import tts


print(f"{config.VA_NAME} (v{config.VA_VER}) начал свою работу ...")


def execute_cmd(command, command_verbose):
    VA_COMMANDS_MAP[command](command_verbose)


def filter_cmd(raw_voice: str) -> Union[str, None]:
    # Обрезает строку, если это была команда -> выдает команду 
    voice_assistant_name = config.VA_NAME.lower()[:-1]
    cmd = re.search(f"({voice_assistant_name}.*?)((\s[^\s]+){{1,3}})", raw_voice)

    if cmd and len(cmd.groups()) >= 2:
        return cmd.group(2)

    return cmd


def va_respond(voice: str, partial: bool = False):
    print(voice)
    command = filter_cmd(voice)

    if command:
        # обращаются к ассистенту
        cmd = recognize_cmd(command)
        print(cmd)

        if cmd['cmd'] not in VA_COMMANDS.keys():
            if not partial:
                tts.va_speak("Я не поняла. Повторите ещё раз.")
            return False
        else:
            execute_cmd(cmd['cmd'], command)
            return True
    return False


def recognize_cmd(cmd: str):
    cmd = cmd.strip()
    commands_list = cmd.split(' ')
    for command_len in range(len(commands_list)):
        command = " ".join(commands_list[:command_len + 1])

        rc = {'cmd': '', 'percent': 0}
        for c, v in VA_COMMANDS.items():

            for x in v:
                vrt = fuzz.ratio(command, x)
                if vrt > rc['percent'] and vrt > config.VA_PRECISION_RATE:
                    rc['cmd'] = c
                    rc['percent'] = vrt
                    return rc
    return rc



def main():
    systray = SysTrayIcon(
        icon="va.ico", 
        hover_text=f"{config.VA_NAME} слушает вас", 
        on_quit=va_exit
    )
    systray.start()

    # начать прослушивание команд
    stt.va_listen(va_respond)


if __name__ == "__main__":
    main()
