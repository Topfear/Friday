import datetime
import tts
from num2t4ru import num2text


def say_time(command_verbose):
    # current time
    now = datetime.datetime.now()
    text = f"Сейчас {num2text(now.hour)} часов {num2text(now.minute)} минут"
    tts.va_speak(text)
