import re
from time import sleep
import webbrowser
import pyautogui, pyperclip
import tts


# qwerty = "qwertyuiop[]asdfghjkl;'zxcvbnm,."
# ycuken = "йцукенгшщзхъфывапролджэячсмитьбю"

# # join as keys and values
# tr = dict(zip(ycuken, qwerty))

# def translate(key):
#     """Returns qwerty key or the given key itself if no mapping found"""
#     return "".join(map(lambda x: tr.get(x.lower(), x), key))


# def translate_sentence(text):
#     result = []
#     for ch in text:
#         result.append(translate(ch))
#     return ''.join(result)

def fast_type(text: str):    
    pyperclip.copy(text)
    pyautogui.hotkey('ctrl', 'v')


def youtube_search(comm: str):
    regex = r'ютуб.*?\s(.*)'
    search = re.search(regex, comm)
    if search:
        webbrowser.open("https://www.youtube.com/", new=2)
        sleep(4)
        pyautogui.press('tab', presses=4, interval=0.2)
        
        request = search.groups()[0]

        sleep(3)
        print(request)
        fast_type(request)
        pyautogui.press('enter')
        pyautogui.press('tab', presses=1, interval=0.1)
        pyautogui.press('enter')
    else:
        tts.va_speak("Повторите пожалуйста запрос")

