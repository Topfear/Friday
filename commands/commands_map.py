from commands.change_voice import change_voice
from commands.code import code
from commands.leetcode import open_leetcode
from commands.open_email import open_email
from commands.open_game import open_game
from commands.open_youtube import open_youtube
from commands.press_space import press_space
from commands.say_hello import say_hello
from commands.say_time import say_time
from commands.shutdown import system_sutdown
from commands.todoist import todoist
from commands.va_exit import va_exit


VA_COMMANDS_MAP = {
    "hello": say_hello,
    "time": say_time,
    "youtube": open_youtube,
    "exit": va_exit,
    "space": press_space,
    "system_sutdown": system_sutdown,
    "code": code,
    "leetcode": open_leetcode,
    "game": open_game,
    "email": open_email,
    "change_voice": change_voice,
    "todoist": todoist,
}

VA_COMMANDS = {
    "hello": ("привет", "приветствую", "здарова"),
    "time": ("сколько время", "сколько времени", "сколько часов"),
    "youtube": ("открой ютуб", "открой ютьуб", "покажи ютуб", "раскрой ютуб"),
    "exit": ("хватит подслушивать", "не подслушивай", "не слушай", "выключись"),
    "space": ("нажми пробел", "пауза", "поставь на паузу", "играй", "продолжай"),
    "system_sutdown": ("выключи ноут", "выключи комп", "выключи компьютер"),
    "code": ("включи вс код", "включи иде", "включи идэе", "открой вс код", "открой идэе", "открой иде"),
    "leetcode": ("ща будем кодить", "алгоритмы", "кодить алгоритмы"),
    "game": ("поиграем", "давай играть", "игра", "играем", "хочу играть", "игрушка"),
    "email": ("почта", "открой почту", "письма", "посмотрим письма", "открой имэил"),
    "change_voice": ("другой голос", "голос"),
    "todoist": ("тудуист", "задачи", "дела", "список дел"),
}
