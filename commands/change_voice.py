import config
import random
import tts

def change_voice(comm=None):
    voices = ['aidar', 'baya', 'kseniya', 'xenia']
    voices.remove(config.VA_SPEAKER)
    config.VA_SPEAKER = random.choice(voices)
    tts.va_speak("Голос сменён")
