# Голосовой ассистент "Пятница"

Голосовой ассистент может выполнять голосовые команды.  
Доступные команды:

- Пятница, привет
- Пятница, сколько время
- Пятница, открой ютуб
- Пятница, хватит подслушивать (голосовой ассистент завершит работу)
- Пятница, пауза/играй (нажмёт пробел)
- Пятница, выключи ноут
- Пятница, включи иде (включает VS Code)
- Пятница, алгоритмы (открывает leetcode)
- Пятница, поиграем (открывает игру)
- Пятница, почта (открывает yandex mail)
- Пятница, другой голос (сменит голос)
- Пятница, список дел (открывает todoist)

---

## Установка

Для установки необходимо:

1. Скачать python [https://www.python.org/downloads/](https://www.python.org/downloads/ "Скачать python")
2. Скачать проект ```git clone https://gitlab.com/Topfear/friday.git```
3. Запустить файл ```build_env.bat```
4. Запустить файл ```build_env_dev.bat```
5. Запустить файл ```build_exe.bat```
6. Экзешник будет лежать в папке ```/dist```

---

## TODO

- Выключать комп только от имени администратора
- Красиво выключать комп
- Прибавить/убавить звук компа
- Прибрать код по папкам
- Добавить команды
  - Добавить задачу в todoist
  - Свернуть все окна
  - Перезагрузить ноут
- Найти замену модулю sounddevice
- Скомпилировать в exe файл
