import os
import torch
import sounddevice as sd
import time

from config import get_speaker


language = 'ru'
model_id = 'ru_v3'
sample_rate = 48000 # 48000
put_accent = True
put_yo = True
device = torch.device('cpu') # cpu или gpu
local_file = 'model.pt'

if not os.path.isfile(local_file):
    torch.hub.download_url_to_file('https://models.silero.ai/models/tts/ru/ru_v3.pt',
                                   local_file)  

model = torch.package.PackageImporter(local_file).load_pickle("tts_models", "model")
model.to(device)

# воспроизводим
def va_speak(what: str):
    audio = model.apply_tts(text=what,
                            speaker=get_speaker(),
                            sample_rate=sample_rate,
                            put_accent=put_accent,
                            put_yo=put_yo)

    sd.play(audio, sample_rate * 1.05)
    time.sleep((len(audio) / sample_rate) + 0.5)
    sd.stop()
