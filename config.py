VA_NAME = "Деметра"
VA_VER = 1


VA_PRECISION_RATE = 60

VA_SPEAKER = 'baya' # aidar, baya, kseniya, xenia, random

def set_speaker(voice):
    global VA_SPEAKER
    VA_SPEAKER = voice

def get_speaker():
    return VA_SPEAKER
